import React from "react";
import { useEffect, useState } from "react";

const App = ({}) => {

    const [search, setSearch] = useState('');
    const [movies, setMovies] = useState([]);
    const [alert, setAlert] = useState('');

    useEffect(() => {

        if (search.length === 0) {
            setAlert('');
            setMovies([]);
            return;
        }

        seachMovies(search)
        .then((body) => {

            if (body.Response === "False") {
                setAlert(body.Error);
                setMovies([]);
            } else {
                setAlert('');
                setMovies([...body.Search]);
            }
        });

    }, [search])

    return (
        <div>
            <Alert message={alert} />

            <input type="text" value={search} onChange={(evt) => setSearch(evt.target.value)} />

            {(movies.length === 0) ?
                <div>no movies</div> 
                : 
                <MovieList movies={movies} />
            }
        </div>
    );

}

export default App;

const seachMovies = (search)  => {
    return fetch('https://www.omdbapi.com/?s=' + search + '&apikey=d16c98cd')
    .then((response) => response.json())
    .then((data) => data);
}


const MovieList = ({movies}) => {

    const movieElms = movies.map((movie, i) => {
        return <MovieDisplay movie={movie} key={i} btnLabel="Does Nothing"/>
    });

    return (
        <table style={{ width: "100%" }}>
            <thead>
                <tr>
                    <th>Posteer</th>
                    <th>Title</th>
                    <th>Year</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {movieElms}
            </tbody>

        </table>
    );

}

const MovieDisplay = ({movie, btnLabel}) => {

    const poster = movie.Poster === "N/A" ? "n/a" : <img style={{ width: "150px" }} src={movie.Poster} />;

    return (
        <tr>
            <td>{poster}</td>
            <td>{movie.Title}</td>
            <td>{movie.Year}</td>
            <td><button>{btnLabel}</button></td>
        </tr>
    )
}

const Alert = ({message}) => {

    return (
        <div>
            {message}
        </div>
    );
}